// Some fake data generated from the faker api.

const faker = require("faker");
const fs = require("fs");
const capitalize = require("./lib/capitalize");
const randomFill = require("./lib/randomFill");

const numberOfSlides = 1;

let barCsv = "";
const barPath = "./data/bar.csv";
let lineCsv = "";
const linePath = "./data/line.csv";
let pieCsv = "";
const piePath = "./data/pie.csv";
let horzBarCsv = "";
const horzBarPath = "./data/horzBar.csv";

const dataPath = "./data/test-chart.json";
const data = {};
data.slides = [];

for (let i = 0; i < numberOfSlides; i++) {
  const entry = {};
  entry.pageTitle = faker.company.catchPhrase();

  /* BAR CHART */
  const bar = {};
  bar.file_name = barPath;

  const prodName1 = capitalize(faker.commerce.product());
  const prodAmt1 = faker.random.number(10);
  const prodName2 = capitalize(faker.commerce.product());
  const prodAmt2 = faker.random.number(10);
  const prodName3 = capitalize(faker.commerce.product());
  const prodAmt3 = faker.random.number(10);
  const prodName4 = capitalize(faker.commerce.product());
  const prodAmt4 = faker.random.number(10);

  // CSV file
  barCsv += `Product,Amount\n`;
  barCsv += `${prodName1},${prodAmt1}\n`;
  barCsv += `${prodName2},${prodAmt2}\n`;
  barCsv += `${prodName3},${prodAmt3}\n`;
  barCsv += `${prodName4},${prodAmt4}\n`;

  entry.bar = bar;

  /* LINE CHART */
  const line = {};
  line.file_name = linePath;

  const colorName1 = capitalize(faker.commerce.color());
  const colorName2 = capitalize(faker.commerce.color());
  const colorName3 = capitalize(faker.commerce.color());

  const color1Data = randomFill(4, 10);
  const color2Data = randomFill(4, 10);
  const color3Data = randomFill(4, 10);
  const color4Data = randomFill(4, 10);

  // CSV file
  lineCsv += `Date,${colorName1},${colorName2},${colorName3}\n`;
  lineCsv += `Q1,${color1Data[0]},${color2Data[0]},${color3Data[0]}\n`;
  lineCsv += `Q2,${color1Data[1]},${color2Data[1]},${color3Data[1]}\n`;
  lineCsv += `Q3,${color1Data[2]},${color2Data[2]},${color3Data[2]}\n`;
  lineCsv += `Q4,${color1Data[3]},${color2Data[3]},${color3Data[3]}\n`;

  entry.line = line;

  /* PIE CHART */
  const pie = {};
  pie.file_name = piePath;

  const blueberry = faker.random.number(30);
  const rhubarb = faker.random.number(30);
  const pumpkin = faker.random.number(30);
  const cheesecake = 100 - blueberry - rhubarb - pumpkin;

  // CSV file too...
  pieCsv += `Type,Amount\n`;
  pieCsv += `Blueberry,${blueberry}\n`;
  pieCsv += `Rhubarb,${rhubarb}\n`;
  pieCsv += `Pumpkin,${pumpkin}\n`;
  pieCsv += `Cheesecake,${cheesecake}\n`;

  entry.pie = pie;

  /* HORIZONTAL BAR CHART */
  const horzBar = {};
  horzBar.file_name = horzBarPath;

  const action1 = faker.hacker.ingverb();
  const action2 = faker.hacker.ingverb();
  const action3 = faker.hacker.ingverb();
  const action1Data = randomFill(3, 100);
  const action2Data = randomFill(3, 100);
  const action3Data = randomFill(3, 100);
  const action4Data = randomFill(3, 100);

  // CSV file too...
  horzBarCsv += `Date,${action1},${action2},${action3}\n`;
  horzBarCsv += `Q1,${action1Data[0]},${action1Data[1]},${action1Data[2]}\n`;
  horzBarCsv += `Q2,${action2Data[0]},${action2Data[1]},${action2Data[2]}\n`;
  horzBarCsv += `Q3,${action3Data[0]},${action3Data[1]},${action3Data[2]}\n`;
  horzBarCsv += `Q4,${action4Data[0]},${action4Data[1]},${action4Data[2]}\n`;

  entry.horzBar = horzBar;

  data.slides.push(entry);
}

console.dir(data, { depth: null });

fs.writeFileSync(barPath, barCsv);
fs.writeFileSync(linePath, lineCsv);
fs.writeFileSync(piePath, pieCsv);
fs.writeFileSync(horzBarPath, horzBarCsv);
fs.writeFileSync(dataPath, JSON.stringify(data));
