// Some fake data generated from the faker api.

const faker = require("faker");
const fs = require("fs");

const capitalize = require("./lib/capitalize");

const numberOfSlides = 1;
const data = {};
data.slides = [];

for (let i = 0; i < numberOfSlides; i++) {
  const entry = {};
  entry.greeting = `${capitalize(
    faker.company.bsAdjective()
  )} ${faker.company.bsNoun()}`;
  entry.title = faker.name.title();
  entry.name = faker.name.findName();
  entry.catchPhrase = faker.hacker.phrase();

  data.slides.push(entry);
}

console.dir(data);

fs.writeFileSync("./data/test-data.json", JSON.stringify(data));
