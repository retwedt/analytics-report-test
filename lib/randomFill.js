const faker = require("faker");

/**
 * Fill an array with random values.
 *
 * @param {*} num
 * @param {*} maxValue
 */
const randomFill = (num, maxValue) => {
  const arr = [];
  for (let i = 0; i < num; i++) {
    arr.push(faker.random.number(maxValue));
  }
  return arr;
};

module.exports = randomFill;
